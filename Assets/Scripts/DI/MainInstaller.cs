﻿using Ld42;
using Ld42.Gui;
using Zenject;

namespace DI {
    public class MainInstaller : MonoInstaller {

        public override void InstallBindings() {
            Container.Bind<GuiController>().FromNewComponentOnNewGameObject().AsSingle().NonLazy();

            Container.Bind<Inventory>().AsSingle().NonLazy();
            Container.Bind<CraftingManager>().AsSingle().NonLazy();

            PrefabsInstaller.Install(Container);
            DataInstaller.Install(Container);
        }

    }
}