﻿using Ld42.Data;
using Zenject;

namespace DI {
    public class DataInstaller : Installer<DataInstaller> {

        public override void InstallBindings() {
            Container.Bind<ItemsInfo>().FromResource("Data/ItemsInfo");
            Container.Bind<CraftsInfo>().FromResource("Data/CraftsInfo");
        }

    }
}