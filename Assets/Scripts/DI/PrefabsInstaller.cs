﻿using Jir.Prefabs;
using Ld42;
using Ld42.Gui.InventoryGui;
using Zenject;

namespace DI {
    public class PrefabsInstaller : Installer<PrefabsInstaller> {
        private const string PrefabsFolder = "Prefabs/";

        public override void InstallBindings() {
            BindPrefab<InventorySlotIcon>("InventorySlotIcon").AsSingle().NonLazy();
            BindPrefab<CraftRecipeIcon>("CraftRecipeIcon").AsSingle().NonLazy();
            BindPrefab<CraftIngredientIcon>("CraftIngredientIcon").AsSingle().NonLazy();
            BindPrefab<PlacingSpot>("PlacingSpot").AsSingle().NonLazy();
        }

        private ScopeConcreteIdArgConditionCopyNonLazyBinder BindPrefab<T>(string path) {
            return Container.Bind<Prefab<T>>().FromMethod(context => CreatePrefab<T>(context, PrefabsFolder + path));
        }

        private ScopeConcreteIdArgConditionCopyNonLazyBinder BindPrefab<T>(string path, string id) {
            return Container.Bind<Prefab<T>>().WithId(id).FromMethod(context => CreatePrefab<T>(context, PrefabsFolder + path));
        }

        private static Prefab<T> CreatePrefab<T>(InjectContext injectContext, string path) {
            var instantiator = injectContext.Container.Resolve<IInstantiator>();
            return new Prefab<T>(instantiator, path);
        }
    }
}