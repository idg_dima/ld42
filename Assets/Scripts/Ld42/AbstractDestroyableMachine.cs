﻿using System;
using Ld42.Data;
using UnityEngine;

namespace Ld42 {
    public abstract class AbstractDestroyableMachine : MonoBehaviour, IInteractable {
        private const float TimeToDestroy = 1.5f;

        public event Action MachineDestroyedEvent = () => { };

        public virtual InteractionType GetInteractionType(InventorySlot selectedItem) {
            return CanDestroyWith(selectedItem) ? InteractionType.Extract : InteractionType.None;
        }

        protected bool CanDestroyWith(InventorySlot selectedItem) {
            if (selectedItem == null) {
                return false;
            }
            return selectedItem.itemId == ItemIds.StonePickaxe || selectedItem.itemId == ItemIds.IronPickaxe;
        }

        public virtual string GetActionName(InventorySlot selectedItem) {
            return CanDestroyWith(selectedItem) ? "Destroy machine" : "";
        }

        public virtual float GetInteractionTime(InventorySlot selectedItem) {
            return CanDestroyWith(selectedItem) ? TimeToDestroy : 0;
        }

        public virtual void Interact(InventorySlot selectedItem) {
            if (CanDestroyWith(selectedItem) && GiveDestroyResources()) {
                MachineDestroyedEvent();
            }
        }

        protected abstract bool GiveDestroyResources();
    }
}