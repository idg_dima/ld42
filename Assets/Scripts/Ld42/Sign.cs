﻿using Ld42.Data;
using TMPro;
using UnityEngine;
using Zenject;

namespace Ld42 {
    public class Sign : MonoBehaviour, IInteractable {
        [Inject] private Inventory _inventory;
        [Inject] private ItemsInfo _itemsInfo;

        [SerializeField] private SpriteRenderer _spriteRenderer;
        [SerializeField] private TextMeshPro _textMesh;
        [SerializeField] private ItemIds _priceItemId;
        [SerializeField] private int _priceItemCount;
        [SerializeField] private GameObject _objectToUnlock;

        private void Awake() {
            _spriteRenderer.sprite = _itemsInfo.GetInfo(_priceItemId).Sprite;
            _textMesh.text = "x " + _priceItemCount;
        }

        public InteractionType GetInteractionType(InventorySlot selectedItem) {
            return InteractionType.Use;
        }

        public float GetInteractionTime(InventorySlot selectedItem) {
            return 0;
        }

        public void Interact(InventorySlot selectedItem) {
            if (_inventory.HasItem(_priceItemId, _priceItemCount)) {
                _inventory.RemoveItem(_priceItemId, _priceItemCount);
                gameObject.SetActive(false);
                _objectToUnlock.SetActive(true);
            }
        }

        public string GetActionName(InventorySlot selectedItem) {
            return "Unlock";
        }
    }
}