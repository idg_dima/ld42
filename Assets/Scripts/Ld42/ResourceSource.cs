﻿using Ld42.Data;
using UnityEngine;
using Zenject;

namespace Ld42 {
    public class ResourceSource : MonoBehaviour, IInteractable {
        [Inject] private Inventory _inventory;
        [Inject] private ItemsInfo _itemsInfo;

        [SerializeField] private ItemIds _itemId;
        [SerializeField] private int _itemsCount;
        [SerializeField] private int _interactionTime = 1;
        [SerializeField] private ToolTypes _requiredTool = ToolTypes.None;

        public string GetActionName(InventorySlot selectedItem) {
            return "Collect";
        }

        public InteractionType GetInteractionType(InventorySlot selectedItem) {
            return InteractionType.Extract;
        }

        public float GetInteractionTime(InventorySlot selectedItem) {
            if (selectedItem == null || _requiredTool == ToolTypes.None) {
                return _interactionTime;
            }

            var itemInfo = _itemsInfo.GetInfo(selectedItem.itemId);
            var efficiency = itemInfo.ToolType == _requiredTool ? itemInfo.ToolEfficiency : 1;
            return _interactionTime / efficiency;
        }

        public void Interact(InventorySlot selectedItem) {
            _inventory.AddItem(_itemId, _itemsCount);
        }

        public ItemIds ItemId {
            get { return _itemId; }
        }
    }
}