﻿using System.Collections;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;
using Zenject;

namespace Ld42 {
    public class Player : MonoBehaviour {
        private FirstPersonController _fpsController;

        public const float InteractDistance = 3;

        [Inject] private Camera _camera;
        [Inject] private Inventory _inventory;

        [SerializeField] private PlayerToolContainer _toolContainer;
        [SerializeField] private Transform _respawnPoint;

        public IInteractable InteractableTarget { get; private set; }
        public bool IsInteracting { get; private set; }
        public float InteractionProgress { get; private set; }

        private int _hitMask;
        private Coroutine _interactCoroutine;
        private bool _isPaused;

        private void Awake() {
            _fpsController = GetComponent<FirstPersonController>();
            _hitMask = LayerMask.GetMask("Obstacles", "Interactive", "PlacingSpots");
        }

        public bool Paused {
            set {
                _isPaused = value;
                _fpsController.enabled = !value;
                _fpsController.MouseLook.SetCursorLock(!value);

                if (_isPaused) {
                    InteractableTarget = null;
                    StopInteraction();
                }
            }
        }

        private void Update() {
            if (transform.position.y < -10) {
                transform.position = new Vector3(_respawnPoint.position.x, _respawnPoint.position.y, _respawnPoint.position.z);
            }

            if (_isPaused)
                return;

            var oldTarget = InteractableTarget;

            RaycastHit hitInfo;
            if (Physics.Raycast(_camera.transform.position, _camera.transform.forward, out hitInfo, InteractDistance, _hitMask)) {
                InteractableTarget = hitInfo.collider.gameObject.GetComponent<IInteractable>();
            } else {
                InteractableTarget = null;
            }

            if (IsInteracting && oldTarget != InteractableTarget || Input.GetMouseButtonUp(0) || Input.GetKeyUp(KeyCode.E)) {
                StopInteraction();
            }

            if (!IsInteracting && InteractableTarget != null) {
                var selectedItem = _inventory.GetSelectedHotbarItem();

                var interactionType = InteractableTarget.GetInteractionType(selectedItem);
                if ((interactionType == InteractionType.Extract || interactionType == InteractionType.Place) && Input.GetMouseButton(0)) {
                    StartInteraction();
                } else if ((interactionType == InteractionType.Use || interactionType == InteractionType.Take) && Input.GetKeyDown(KeyCode.E)) {
                    StartInteraction();
                }
            }
        }

        private void StartInteraction() {
            if (_interactCoroutine != null) {
                StopCoroutine(_interactCoroutine);
            }

            var selectedItem = _inventory.GetSelectedHotbarItem();
            if (Mathf.Approximately(InteractableTarget.GetInteractionTime(selectedItem), 0)) {
                InteractableTarget.Interact(selectedItem);
            } else {
                _interactCoroutine = StartCoroutine(InteractCoroutine());
            }
        }

        private void StopInteraction() {
            if (_interactCoroutine != null) {
                StopCoroutine(_interactCoroutine);
                _interactCoroutine = null;
            }

            _toolContainer.IsUsing = false;
            IsInteracting = false;
        }

        private IEnumerator InteractCoroutine() {
            _toolContainer.IsUsing = true;
            IsInteracting = true;
            InteractionProgress = 0;
            var selectedItem = _inventory.GetSelectedHotbarItem();
            var interactionTime = InteractableTarget.GetInteractionTime(selectedItem);

            while (InteractionProgress < 1) {
                InteractionProgress = Mathf.Clamp01(InteractionProgress + 1 / interactionTime * Time.deltaTime);
                yield return null;
            }

            _toolContainer.IsUsing = false;
            IsInteracting = false;
            InteractableTarget.Interact(selectedItem);
        }
    }
}