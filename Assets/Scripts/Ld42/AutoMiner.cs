﻿using System.Collections;
using Ld42.Data;
using UnityEngine;
using Zenject;

namespace Ld42 {

    [RequireComponent(typeof(Animator))]
    public class AutoMiner : AbstractDestroyableMachine, IMachineWithInventory {
        private const float RotationTime = 0.15f;

        [Inject] private Inventory _inventory;

        [SerializeField] private Transform _frontRaycastPoint;

        private int _raycastMask;
        private bool _isRotating;
        private int _rotation;
        private ItemIds? _contents;
        private int _animationTrigger = Animator.StringToHash("WorkCycle");
        private Animator _animator;

        private void Awake() {
            _animator = GetComponent<Animator>();

            _raycastMask = LayerMask.GetMask("Interactive");
            StartCoroutine(UpdateCoroutine());
        }

        protected override bool GiveDestroyResources() {
            if (_inventory.CheckFreeSlotsAndTriggerEvent(ItemIds.Furnace, 1)) {
                _inventory.AddItem(ItemIds.AutoMiner, 1);
                return true;
            }

            return false;
        }

        public override InteractionType GetInteractionType(InventorySlot selectedItem) {
            if (CanDestroyWith(selectedItem)) {
                return base.GetInteractionType(selectedItem);
            }

            return InteractionType.Use;
        }

        public override string GetActionName(InventorySlot selectedItem) {
            if (CanDestroyWith(selectedItem)) {
                return base.GetActionName(selectedItem);
            }
            return "Rotate";
        }

        public override void Interact(InventorySlot selectedItem) {
            if (CanDestroyWith(selectedItem)) {
                base.Interact(selectedItem);
                return;
            }

            if (!_isRotating) {
                StartCoroutine(RotateCoroutine());
            }
        }

        private IEnumerator RotateCoroutine() {
            _isRotating = true;

            var timer = 0f;
            while (timer < RotationTime) {
                timer += Time.deltaTime;
                var angle = Mathf.Lerp(_rotation, _rotation + 1, timer / RotationTime) * 90;
                transform.localRotation = Quaternion.Euler(0, angle, 0);
                yield return null;
            }

            _rotation++;
            _isRotating = false;
        }

        private IEnumerator UpdateCoroutine() {
            while (true) {
                if (!_isRotating && _contents == null) {
                    var frontSource = GetFrontSource();
                    if (frontSource != null) {
                        _contents = frontSource.ItemId;
                        _animator.SetTrigger(_animationTrigger);
                    }
                }

                yield return new WaitForSeconds(1);
            }
        }

        private ResourceSource GetFrontSource() {
            RaycastHit hitInfo;
            if (Physics.Raycast(_frontRaycastPoint.position, transform.forward, out hitInfo, 0.1f, _raycastMask)) {
                return hitInfo.collider.gameObject.GetComponent<ResourceSource>();
            }

            return null;
        }

        public bool CanTakeOneItem() {
            return _contents != null;
        }

        public ItemIds TakeOneItem() {
            var itemId = _contents ?? ItemIds.Stone;
            _contents = null;
            return itemId;
        }

        public bool CanPlaceOneItem(ItemIds itemId) {
            return false;
        }

        public void PlaceOneItem(ItemIds itemId) {

        }
    }
}