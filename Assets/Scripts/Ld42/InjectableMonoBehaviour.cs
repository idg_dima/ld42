﻿using UnityEngine;

namespace Ld42 {
    public class InjectableMonoBehaviour : MonoBehaviour {
        private bool _started;
        protected bool IsEnabled;

        protected virtual void Start() {
            _started = true;
            if (IsEnabled) {
                OnEnableInjected();
            }
        }

        protected void OnEnable() {
            if (_started) {
                OnEnableInjected();
            }
            IsEnabled = true;
        }

        /// <summary>
        /// This method is called the same way as OnEnable, but it is called only when injection is complete
        /// </summary>
        protected virtual void OnEnableInjected() {
        }

        protected virtual void OnDisable() {
            IsEnabled = false;
        }
    }
}