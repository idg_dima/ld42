﻿using System.Collections.Generic;
using Jir.Prefabs;
using Ld42.Data;
using UnityEngine;
using Zenject;

namespace Ld42 {
    public class PlacingSpotsGroup : InjectableMonoBehaviour {
        [Inject] private Prefab<PlacingSpot> _placingSpotPrefab;
        [Inject] private ItemsInfo _itemsInfo;
        [Inject] private Inventory _inventory;

        private List<PlacingSpot> _placingSpots;
        private bool _areSpotsVisible = true;

        private void Awake() {
            CreateSpots();
        }

        private void CreateSpots() {
            _placingSpots = new List<PlacingSpot>();

            // island 1
            AddSpot(-1, -1);
            AddSpot( 0, -1);
            AddSpot( 1, -1);
            AddSpot( 0,  0);
            AddSpot( 1,  0);
            AddSpot(-1,  1);
            AddSpot( 0,  1);
            AddSpot( 1,  1);

            // island 2
            AddSpot( 0, -1 - 10);
            AddSpot( 1, -1 - 10);
            AddSpot(-1,  0 - 10);
            AddSpot( 0,  0 - 10);
            AddSpot( 1,  0 - 10);
            AddSpot(-1,  1 - 10);
            AddSpot( 0,  1 - 10);
            AddSpot( 1,  1 - 10);

            // island 3
            AddSpot( 0 + 10, -1 - 10);
            AddSpot( 1 + 10, -1 - 10);
            AddSpot( 2 + 10, -1 - 10);
            AddSpot(-1 + 10,  0 - 10);
            AddSpot( 0 + 10,  0 - 10);
            AddSpot( 1 + 10,  0 - 10);
            AddSpot( 2 + 10,  0 - 10);
            AddSpot( 3 + 10,  0 - 10);
            AddSpot(-1 + 10,  1 - 10);
            AddSpot( 0 + 10,  1 - 10);
            AddSpot( 1 + 10,  1 - 10);
            AddSpot( 2 + 10,  1 - 10);
            AddSpot( 3 + 10,  1 - 10);

            // island 4
            AddSpot(-1 + 10, -1);
            AddSpot( 0 + 10, -1);
            AddSpot( 1 + 10, -1);
            AddSpot(-1 + 10,  0);
            AddSpot( 0 + 10,  0);
            AddSpot( 1 + 10,  0);
            AddSpot(-1 + 10,  1);
            AddSpot( 0 + 10,  1);
            AddSpot( 1 + 10,  1);

            // island 5
            AddSpot(-2 + 21, -2 + 1);
            AddSpot(-1 + 21, -2 + 1);
            AddSpot( 0 + 21, -2 + 1);
            AddSpot( 1 + 21, -2 + 1);
            AddSpot( 2 + 21, -2 + 1);
            AddSpot(-2 + 21, -1 + 1);
            AddSpot(-1 + 21, -1 + 1);
            AddSpot( 0 + 21, -1 + 1);
            AddSpot( 1 + 21, -1 + 1);
            AddSpot( 2 + 21, -1 + 1);
            AddSpot(-2 + 21,  0 + 1);
            AddSpot(-1 + 21,  0 + 1);
            AddSpot( 0 + 21,  0 + 1);
            AddSpot( 1 + 21,  0 + 1);
            AddSpot( 2 + 21,  0 + 1);
            AddSpot(-2 + 21,  1 + 1);
            AddSpot(-1 + 21,  1 + 1);
            AddSpot( 0 + 21,  1 + 1);
            AddSpot( 1 + 21,  1 + 1);
            AddSpot( 2 + 21,  1 + 1);
            AddSpot(-2 + 21,  2 + 1);
            AddSpot(-1 + 21,  2 + 1);
            AddSpot( 0 + 21,  2 + 1);
            AddSpot( 1 + 21,  2 + 1);
            AddSpot( 2 + 21,  2 + 1);

            // island 6
            AddSpot(-2 + 21, -2 - 11);
            AddSpot(-1 + 21, -2 - 11);
            AddSpot( 0 + 21, -2 - 11);
            AddSpot( 1 + 21, -2 - 11);
            AddSpot( 2 + 21, -2 - 11);
            AddSpot(-2 + 21, -1 - 11);
            AddSpot(-1 + 21, -1 - 11);
            AddSpot( 0 + 21, -1 - 11);
            AddSpot( 1 + 21, -1 - 11);
            AddSpot( 2 + 21, -1 - 11);
            AddSpot(-2 + 21,  0 - 11);
            AddSpot(-1 + 21,  0 - 11);
            AddSpot( 0 + 21,  0 - 11);
            AddSpot( 1 + 21,  0 - 11);
            AddSpot( 2 + 21,  0 - 11);
            AddSpot(-2 + 21,  1 - 11);
            AddSpot(-1 + 21,  1 - 11);
            AddSpot( 0 + 21,  1 - 11);
            AddSpot( 1 + 21,  1 - 11);
            AddSpot( 2 + 21,  1 - 11);
            AddSpot(-2 + 21,  2 - 11);
            AddSpot(-1 + 21,  2 - 11);
            AddSpot( 0 + 21,  2 - 11);
            AddSpot( 1 + 21,  2 - 11);
            AddSpot( 2 + 21,  2 - 11);

        }

        private void AddSpot(int x, int z) {
            var spot = _placingSpotPrefab.Instantiate();
            spot.transform.parent = transform;
            spot.transform.localPosition = new Vector3(x, 0, z);
            _placingSpots.Add(spot);
        }

        protected override void OnEnableInjected() {
            base.OnEnableInjected();

            UpdateVisibility();
            _inventory.InventoryChangedEvent += UpdateVisibility;
        }

        protected override void OnDisable() {
            base.OnDisable();

            _inventory.InventoryChangedEvent -= UpdateVisibility;
        }

        private void UpdateVisibility() {
            var newVisible = NeedToShowSpots();
            if (newVisible == _areSpotsVisible) {
                return;
            }

            _areSpotsVisible = newVisible;
            foreach (var spot in _placingSpots) {
                spot.OutlineVisible = _areSpotsVisible;
            }
        }

        private bool NeedToShowSpots() {
            var item = _inventory.GetSelectedHotbarItem();
            if (item == null) {
                return false;
            }

            var itemInfo = _itemsInfo.GetInfo(item.itemId);
            return itemInfo.PlacedPrefab != null;
        }
    }
}