﻿using Ld42.Data;

namespace Ld42 {
    public interface IMachineWithInventory {
        bool CanTakeOneItem();

        ItemIds TakeOneItem();

        bool CanPlaceOneItem(ItemIds itemId);

        void PlaceOneItem(ItemIds itemId);
    }
}