﻿using Ld42.Data;
using TMPro;
using UnityEngine;
using Zenject;

namespace Ld42 {
    public class CrateItemsInfo : MonoBehaviour {

        [Inject] private ItemsInfo _itemsInfo;

        [SerializeField] private SpriteRenderer _spriteRenderer;
        [SerializeField] private TextMeshPro _textMesh;

        private bool _isVisible;
        private bool _hasItem;

        public bool IsVisible {
            set {
                _isVisible = value;
                UpdateVisibility();
            }
        }

        public void UpdateItem(InventorySlot itemSlot) {
            _hasItem = itemSlot != null;
            if (itemSlot != null) {
                var itemInfo = _itemsInfo.GetInfo(itemSlot.itemId);
                _spriteRenderer.sprite = itemInfo.Sprite;
                _textMesh.text = itemSlot.count.ToString();
            }

            UpdateVisibility();
        }

        private void UpdateVisibility() {
            gameObject.SetActive(_isVisible && _hasItem);
        }
    }
}