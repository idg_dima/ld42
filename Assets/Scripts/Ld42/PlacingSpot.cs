﻿using Ld42.Data;
using UnityEngine;
using Zenject;

namespace Ld42 {

    [RequireComponent(typeof(BoxCollider))]
    public class PlacingSpot : MonoBehaviour, IInteractable {
        [Inject] private ItemsInfo _itemsInfo;
        [Inject] private Inventory _inventory;
        [Inject] private IInstantiator _instantiator;

        [SerializeField] private GameObject _emptySpot;

        private BoxCollider _boxCollider;
        private GameObject _placedMachine;
        private bool _isOutlineVisible = true;

        private void Awake() {
            _boxCollider = GetComponent<BoxCollider>();
        }

        public bool OutlineVisible {
            set {
                _isOutlineVisible = value;
                UpdateOutline();
            }
        }

        private void UpdateOutline() {
            _emptySpot.SetActive(_placedMachine == null && _isOutlineVisible);
            _boxCollider.enabled = _placedMachine == null && _isOutlineVisible;
        }

        public InteractionType GetInteractionType(InventorySlot selectedItem) {
            return InteractionType.Place;
        }

        public string GetActionName(InventorySlot selectedItem) {
            return "Place";
        }

        public float GetInteractionTime(InventorySlot selectedItem) {
            return 0;
        }

        public void Interact(InventorySlot selectedItem) {
            if (selectedItem == null) {
                return;
            }

            _inventory.RemoveItem(selectedItem.itemId, 1);

            var itemInfo = _itemsInfo.GetInfo(selectedItem.itemId);
            var instance = _instantiator.InstantiatePrefab(itemInfo.PlacedPrefab);
            instance.transform.SetParent(transform, false);
            instance.transform.localPosition = Vector3.zero;
            instance.transform.localRotation = Quaternion.identity;

            var destroyableMachine = instance.GetComponent<AbstractDestroyableMachine>();
            if (destroyableMachine != null) {
                destroyableMachine.MachineDestroyedEvent += DestroyMachine;
            }

            _placedMachine = instance;
            UpdateOutline();
        }

        private void DestroyMachine() {
            if (!_placedMachine) {
                return;
            }

            Destroy(_placedMachine);
            UpdateOutline();
        }
    }
}