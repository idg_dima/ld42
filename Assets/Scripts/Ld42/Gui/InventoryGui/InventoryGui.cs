﻿using Jir.Prefabs;
using UnityEngine;
using Zenject;

namespace Ld42.Gui.InventoryGui {
    public class InventoryGui : MonoBehaviour {
        [Inject] private Prefab<InventorySlotIcon> _slotPrefab;
        [Inject] private Inventory _inventory;
        [Inject] private ItemTooltip _itemTooltip;

        [SerializeField] private Transform _slotsContainer;
        [SerializeField] private InventorySlotIcon _draggingSlotIcon;
        [SerializeField] private RectTransform _draggingSlotTransform;
        [SerializeField] private RectTransform _canvasTransform;
        private InventorySlotIcon[] _slotIcons;

        private void Awake() {
            _slotIcons = new InventorySlotIcon[Inventory.SlotsNum - Inventory.HotbarSize];

            for (var i = Inventory.HotbarSize; i < Inventory.SlotsNum; i++) {
                var slot = _slotPrefab.Instantiate();
                slot.SlotIndex = i;
                slot.SlotClickedEvent += OnAnySlotClicked;
                slot.transform.SetParent(_slotsContainer, false);
                _slotIcons[i - Inventory.HotbarSize] = slot;
            }
        }

        public bool IsVisible {
            set {
                gameObject.SetActive(value);
                if (value) {
                    UpdateSlots();
                    _inventory.InventoryChangedEvent += UpdateSlots;
                } else {
                    _inventory.InventoryChangedEvent -= UpdateSlots;
                    _draggingSlotIcon.gameObject.SetActive(false);
                    _itemTooltip.ClearItem();
                    _inventory.CancelDraggingItem();
                }
            }
        }

        private void UpdateSlots() {
            foreach (var slotIcon in _slotIcons) {
                slotIcon.SlotData = _inventory.GetSlot(slotIcon.SlotIndex);
            }

            _draggingSlotIcon.gameObject.SetActive(_inventory.DraggingSlot != null);
            _draggingSlotIcon.SlotData = _inventory.DraggingSlot;
        }

        private void OnAnySlotClicked(InventorySlotIcon slotIcon, bool isLeftButton) {
            if (_inventory.DraggingSlot == null) {
                _inventory.StartDraggingItem(slotIcon.SlotIndex, isLeftButton);
            } else {
                _inventory.PlaceDraggingItemTo(slotIcon.SlotIndex, isLeftButton);
            }
        }

        private void Update() {
            var screenPoint = Input.mousePosition;
            var position = Camera.main.ScreenToViewportPoint(screenPoint);

            position.x *= _canvasTransform.sizeDelta.x;
            position.y = _canvasTransform.sizeDelta.y * position.y - _canvasTransform.sizeDelta.y;

            _draggingSlotTransform.anchoredPosition = position + new Vector3(52, -60);
        }
    }
}