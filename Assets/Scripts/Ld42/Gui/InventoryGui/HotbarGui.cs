﻿using Jir.Prefabs;
using UnityEngine;
using Zenject;

namespace Ld42.Gui.InventoryGui {
    public class HotbarGui : InjectableMonoBehaviour {
        [Inject] private Prefab<InventorySlotIcon> _slotPrefab;
        [Inject] private Inventory _inventory;
        [Inject] private GuiController _guiController;

        private InventorySlotIcon[] _slotIcons;

        private void Awake() {
            _slotIcons = new InventorySlotIcon[Inventory.HotbarSize];

            for (var i = 0; i < Inventory.HotbarSize; i++) {
                var slot = _slotPrefab.Instantiate();
                slot.SlotIndex = i;
                slot.SlotClickedEvent += OnAnySlotClicked;
                slot.transform.SetParent(transform, false);
                _slotIcons[i] = slot;
            }
        }

        protected override void OnEnableInjected() {
            base.OnEnableInjected();

            UpdateSlots();
            _inventory.InventoryChangedEvent += UpdateSlots;
        }

        protected override void OnDisable() {
            base.OnDisable();

            _inventory.InventoryChangedEvent -= UpdateSlots;
        }

        private void UpdateSlots() {
            foreach (var slotIcon in _slotIcons) {
                slotIcon.SlotData = _inventory.GetSlot(slotIcon.SlotIndex);
                slotIcon.Selected = _inventory.SelectedHotbarSlotIndex == slotIcon.SlotIndex;
            }
        }

        private void OnAnySlotClicked(InventorySlotIcon slotIcon, bool isLeftButton) {
            if (_inventory.DraggingSlot == null) {
                _inventory.StartDraggingItem(slotIcon.SlotIndex, isLeftButton);
            } else {
                _inventory.PlaceDraggingItemTo(slotIcon.SlotIndex, isLeftButton);
            }
        }

        private void Update() {
            for (var i = 0; i < Inventory.HotbarSize; i++) {
                var keyCode = KeyCode.Alpha1 + i;
                if (Input.GetKeyDown(keyCode)) {
                    _inventory.SelectHotbarSlot(i);
                }
            }

            if (!_guiController.IsInventoryOpenned && !Mathf.Approximately(Input.mouseScrollDelta.y, 0)) {
                var ticks = Mathf.FloorToInt(Mathf.Abs(Input.mouseScrollDelta.y));
                var sign = Mathf.Sign(Input.mouseScrollDelta.y);
                var slotIndex = (int) (_inventory.SelectedHotbarSlotIndex - ticks * sign);
                slotIndex = Mathf.Clamp(slotIndex, 0, Inventory.HotbarSize - 1);
                _inventory.SelectHotbarSlot(slotIndex);
            }
        }
    }
}