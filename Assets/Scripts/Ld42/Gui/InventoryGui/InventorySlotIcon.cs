﻿using System;
using JetBrains.Annotations;
using Ld42.Data;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Ld42.Gui.InventoryGui {
    public class InventorySlotIcon : MonoBehaviour {
        [Inject] private ItemsInfo _itemsInfo;
        [Inject] private Inventory _inventory;
        [Inject] private ItemTooltip _itemTooltip;

        [SerializeField] private GameObject _bg;
        [SerializeField] private GameObject _selectedBg;
        [SerializeField] private Image _itemImage;
        [SerializeField] private TextMeshProUGUI _counterText;

        public event Action<InventorySlotIcon, bool> SlotClickedEvent = (s, b) => { };
        public int SlotIndex;

        public InventorySlot SlotData {
            set {
                _itemImage.gameObject.SetActive(value != null);
                _counterText.gameObject.SetActive(value != null);

                if (value != null) {
                    _itemImage.sprite = _itemsInfo.GetInfo(value.itemId).Sprite;
                    _counterText.text = value.count.ToString();
                }
            }
        }

        public bool Selected {
            set {
                _selectedBg.SetActive(value);
                _bg.SetActive(!value);
            }
        }

        [UsedImplicitly]
        public void OnClick() {
            if (Input.GetMouseButtonUp(0)) {
                SlotClickedEvent(this, true);
            } else {
                SlotClickedEvent(this, false);
            }
        }

        [UsedImplicitly]
        public void OnMouseEnter() {
            var itemSlot = _inventory.GetSlot(SlotIndex);
            if (itemSlot == null) {
                _itemTooltip.ClearItem();
            } else {
                _itemTooltip.SetItem(itemSlot.itemId);
            }
        }

        [UsedImplicitly]
        public void OnMouseExit() {
            _itemTooltip.ClearItem();
        }
    }
}