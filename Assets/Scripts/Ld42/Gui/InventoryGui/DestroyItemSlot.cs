﻿using JetBrains.Annotations;
using UnityEngine;
using Zenject;

namespace Ld42.Gui.InventoryGui {
    public class DestroyItemSlot : InjectableMonoBehaviour {
        [Inject] private Inventory _inventory;

        [SerializeField] private GameObject _container;

        protected override void OnEnableInjected() {
            base.OnEnableInjected();

            UpdateVisibility();
            _inventory.InventoryChangedEvent += UpdateVisibility;
        }

        protected override void OnDisable() {
            base.OnDisable();

            _inventory.InventoryChangedEvent -= UpdateVisibility;
        }

        private void UpdateVisibility() {
            _container.SetActive(_inventory.DraggingSlot != null);
        }

        [UsedImplicitly]
        public void OnClick() {
            _inventory.DestroyDraggingItem(Input.GetMouseButtonUp(0));
        }
    }
}