﻿using Ld42.Data;
using TMPro;
using UnityEngine;
using Zenject;

namespace Ld42.Gui.InventoryGui {
    public class ItemTooltip : InjectableMonoBehaviour {
        [Inject] private ItemsInfo _itemsInfo;
        [Inject] private Inventory _inventory;

        [SerializeField] private TextMeshProUGUI _itemNameText;
        [SerializeField] private TextMeshProUGUI _itemDescriptionText;
        [SerializeField] private RectTransform _containerTransform;
        [SerializeField] private RectTransform _canvasTransform;

        private bool _hasItem;
        private RectTransform _rectTransform;

        private void Awake() {
            _rectTransform = GetComponent<RectTransform>();
        }

        protected override void OnEnableInjected() {
            base.OnEnableInjected();

            _inventory.InventoryChangedEvent += UpdateTooltip;
        }

        protected override void OnDisable() {
            base.OnDisable();

            _inventory.InventoryChangedEvent += UpdateTooltip;
        }

        public void SetItem(ItemIds itemId) {
            _hasItem = true;
            var itemInfo = _itemsInfo.GetInfo(itemId);
            _itemNameText.text = itemInfo.Name;
            _itemDescriptionText.text = itemInfo.Description;
            UpdateTooltip();
        }

        public void ClearItem() {
            _hasItem = false;
            UpdateTooltip();
        }

        private void UpdateTooltip() {
            _containerTransform.gameObject.SetActive(_inventory.DraggingSlot == null && _hasItem);
        }

        private void Update() {
            var screenPoint = Input.mousePosition;
            var position = Camera.main.ScreenToViewportPoint(screenPoint);

            position.x *= _canvasTransform.sizeDelta.x;
            position.y = _canvasTransform.sizeDelta.y * position.y - _canvasTransform.sizeDelta.y;

            position.x = Mathf.Clamp(position.x, 0, _canvasTransform.sizeDelta.x - _containerTransform.sizeDelta.x);
            position.y = Mathf.Clamp(position.y, -_canvasTransform.sizeDelta.y + _containerTransform.sizeDelta.y, 0);
            _rectTransform.anchoredPosition = position + new Vector3(0, -2);
        }
    }
}