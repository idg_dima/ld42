﻿using System.Collections;
using Jir.Prefabs;
using Ld42.Data;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Ld42.Gui.InventoryGui {
    public class CraftingListGui : InjectableMonoBehaviour {
        [Inject] private Inventory _inventory;
        [Inject] private CraftsInfo _craftsInfo;
        [Inject] private Prefab<CraftRecipeIcon> _recipePrefab;

        [SerializeField] private ScrollRect _scrollRect;

        private CraftRecipeIcon[] _icons;
        private bool _needToScroll = true;

        private void Awake() {
            var recipes = _craftsInfo.CraftingRecipes;
            _icons = new CraftRecipeIcon[recipes.Length];

            for (var i = 0; i < recipes.Length; i++) {
                var recipeIcon = _recipePrefab.Instantiate();
                recipeIcon.Recipe = recipes[i];
                recipeIcon.transform.SetParent(transform, false);
                _icons[i] = recipeIcon;
            }
        }

        protected override void OnEnableInjected() {
            base.OnEnableInjected();

            UpdateRecipes();
            _inventory.InventoryChangedEvent += UpdateRecipes;
        }

        protected override void OnDisable() {
            base.OnDisable();

            _inventory.InventoryChangedEvent -= UpdateRecipes;
        }

        private void UpdateRecipes() {
            foreach (var craftRecipeIcon in _icons) {
                craftRecipeIcon.UpdateRecipe();
            }
        }

        private void Update() {
            if (_needToScroll) {
                _needToScroll = false;
                _scrollRect.verticalNormalizedPosition = 1;
            }
        }

        private IEnumerator ScrollToTopCoroutine() {
            yield return null;
            yield return null;
            _scrollRect.verticalNormalizedPosition = 1;
        }
    }
}