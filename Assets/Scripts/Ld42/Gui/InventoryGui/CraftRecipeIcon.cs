﻿using JetBrains.Annotations;
using Jir.Prefabs;
using Ld42.Data;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Ld42.Gui.InventoryGui {
    public class CraftRecipeIcon : MonoBehaviour {
        [Inject] private ItemsInfo _itemsInfo;
        [Inject] private Prefab<CraftIngredientIcon> _ingredientPrefab;
        [Inject] private CraftingManager _craftingManager;
        [Inject] private Inventory _inventory;
        [Inject] private ItemTooltip _itemTooltip;

        [SerializeField] private Image _itemImage;
        [SerializeField] private TextMeshProUGUI _counterText;
        [SerializeField] private RectTransform _craftItemsContainer;

        private CraftRecipe _craftRecipe;
        private CraftIngredientIcon[] _ingredientIcons;

        public CraftRecipe Recipe {
            set {
                _craftRecipe = value;

                var itemInfo = _itemsInfo.GetInfo(value.ResultItemId);
                _itemImage.sprite = itemInfo.Sprite;
                _counterText.text = value.ResultItemCount.ToString();

                _ingredientIcons = new CraftIngredientIcon[value.Ingredients.Length];

                for (var i = 0; i < value.Ingredients.Length; i++) {
                    var ingredientIcon = _ingredientPrefab.Instantiate();
                    ingredientIcon.IngredientData = value.Ingredients[i];
                    ingredientIcon.transform.SetParent(_craftItemsContainer, false);
                    ingredientIcon.OnClickEvent += OnClick;
                    _ingredientIcons[i] = ingredientIcon;
                }
            }
        }

        [UsedImplicitly]
        public void OnClick() {
            _craftingManager.Craft(_craftRecipe);
        }

        public void UpdateRecipe() {
            var haveAllItems = true;

            for (var i = 0; i < _craftRecipe.Ingredients.Length; i++) {
                var ingredient = _craftRecipe.Ingredients[i];
                var haveItems = _inventory.HasItem(ingredient.itemId, ingredient.itemCount);
                _ingredientIcons[i].HaveItems = haveItems;
                haveAllItems &= haveItems;
            }

            _itemImage.color = haveAllItems ? Color.white : new Color(1, 1, 1, 0.5f);
        }

        [UsedImplicitly]
        public void OnMouseEnter() {
            _itemTooltip.SetItem(_craftRecipe.ResultItemId);
        }

        [UsedImplicitly]
        public void OnMouseExit() {
            _itemTooltip.ClearItem();
        }
    }
}