﻿using System;
using JetBrains.Annotations;
using Ld42.Data;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Ld42.Gui.InventoryGui {
    public class CraftIngredientIcon : MonoBehaviour {
        [Inject] private ItemsInfo _itemsInfo;
        [Inject] private ItemTooltip _itemTooltip;

        [SerializeField] private Image _itemImage;
        [SerializeField] private TextMeshProUGUI _counterText;

        public event Action OnClickEvent = () => { };

        private ItemIds _itemId;

        public CraftingIngredient IngredientData {
            set {
                _itemId = value.itemId;
                var itemInfo = _itemsInfo.GetInfo(value.itemId);
                _itemImage.sprite = itemInfo.Sprite;
                _counterText.text = value.itemCount.ToString();
            }
        }

        public bool HaveItems {
            set { _counterText.color = value ? Color.white : Color.red; }
        }

        [UsedImplicitly]
        public void OnMouseClick() {
            OnClickEvent();
        }

        [UsedImplicitly]
        public void OnMouseEnter() {
            _itemTooltip.SetItem(_itemId);
        }

        [UsedImplicitly]
        public void OnMouseExit() {
            _itemTooltip.ClearItem();
        }
    }
}