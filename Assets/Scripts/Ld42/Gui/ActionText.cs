﻿using TMPro;
using UnityEngine;
using Zenject;

namespace Ld42.Gui {
    public class ActionText : MonoBehaviour {
        [Inject] private Inventory _inventory;
        [Inject] private Player _player;

        private TextMeshProUGUI _textField;

        private IInteractable _lastTarget;
        private InteractionType _lastInteractionType;

        private void Awake() {
            _textField = GetComponent<TextMeshProUGUI>();
        }

        private void LateUpdate() {
            var selectedItem = _inventory.GetSelectedHotbarItem();
            var interactionType = _player.InteractableTarget == null ? InteractionType.None : _player.InteractableTarget.GetInteractionType(selectedItem);
            if (_lastTarget == _player.InteractableTarget && _lastInteractionType == interactionType) {
                return;
            }

            _lastTarget = _player.InteractableTarget;
            _lastInteractionType = interactionType;

            switch (interactionType) {
                case InteractionType.Extract:
                case InteractionType.Place:
                    _textField.text = "[LMB] " + _player.InteractableTarget.GetActionName(selectedItem);
                    break;
                case InteractionType.Use:
                case InteractionType.Take:
                    _textField.text = "[E] " + _player.InteractableTarget.GetActionName(selectedItem);
                    break;
                default:
                    _textField.text = "";
                    break;
            }
        }
    }
}