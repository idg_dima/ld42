﻿using Ld42.Data;
using Zenject;

namespace Ld42.Gui {
    public class CraftingManager {
        private readonly Inventory _inventory;

        [Inject]
        public CraftingManager(Inventory inventory) {
            _inventory = inventory;
        }

        public void Craft(CraftRecipe recipe) {
            if (_inventory.DraggingSlot != null) {
                _inventory.CancelDraggingItem();
                return;
            }

            if (!CanCraft(recipe) || !_inventory.CheckFreeSlotsAndTriggerEvent(recipe.ResultItemId, recipe.ResultItemCount)) {
                return;
            }

            foreach (var ingredient in recipe.Ingredients) {
                _inventory.RemoveItem(ingredient.itemId, ingredient.itemCount);
            }
            _inventory.AddItem(recipe.ResultItemId, recipe.ResultItemCount);
        }

        private bool CanCraft(CraftRecipe recipe) {
            foreach (var ingredient in recipe.Ingredients) {
                if (!_inventory.HasItem(ingredient.itemId, ingredient.itemCount)) {
                    return false;
                }
            }

            return true;
        }
    }
}