﻿using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Ld42.Gui {
    public class InteractionProgress : MonoBehaviour {
        [Inject] private Player _player;

        [SerializeField] private Image _bgImage;
        [SerializeField] private Image _lineImage;

        private void LateUpdate() {
            IsVisible = _player.IsInteracting;

            if (_player.IsInteracting) {
                _lineImage.fillAmount = _player.InteractionProgress;
            }
        }

        private bool IsVisible {
            set {
                _bgImage.gameObject.SetActive(value);
                _lineImage.gameObject.SetActive(value);
            }
        }

    }
}