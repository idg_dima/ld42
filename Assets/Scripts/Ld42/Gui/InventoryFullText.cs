﻿using System.Collections;
using TMPro;
using UnityEngine;
using Zenject;

namespace Ld42.Gui {

    [RequireComponent(typeof(TextMeshProUGUI))]
    public class InventoryFullText : InjectableMonoBehaviour {

        [Inject] private Inventory _inventory;

        private TextMeshProUGUI _text;

        private void Awake() {
            _text = GetComponent<TextMeshProUGUI>();
            _text.enabled = false;
        }

        protected override void OnEnableInjected() {
            base.OnEnableInjected();

            _inventory.InventoryFullEvent += Show;
        }

        protected override void OnDisable() {
            base.OnDisable();

            _inventory.InventoryFullEvent -= Show;
        }

        public void Show() {
            StopAllCoroutines();
            StartCoroutine(ShowCoroutine());
        }

        private IEnumerator ShowCoroutine() {
            _text.enabled = true;
            yield return new WaitForSeconds(2);
            _text.enabled = false;
        }
    }
}