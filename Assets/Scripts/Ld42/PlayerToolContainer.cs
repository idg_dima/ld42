﻿using Ld42.Data;
using UnityEngine;
using Zenject;

namespace Ld42 {

    [RequireComponent(typeof(Animator))]
    public class PlayerToolContainer : InjectableMonoBehaviour {

        [Inject] private ItemsInfo _itemsInfo;
        [Inject] private Inventory _inventory;
        [Inject] private IInstantiator _instantiator;

        private readonly int _animationParameterHash = Animator.StringToHash("IsActive");
        private Animator _animator;
        private GameObject _currentTool;
        private GameObject _currentToolPrefab;

        private void Awake() {
            _animator = GetComponent<Animator>();
        }

        public bool IsUsing {
            set {
                if (_currentTool != null) {
                    _animator.SetBool(_animationParameterHash, value);
                }
            }
        }

        protected override void OnEnableInjected() {
            base.OnEnableInjected();

            UpdateTool();
            _inventory.InventoryChangedEvent += UpdateTool;
        }

        protected override void OnDisable() {
            base.OnDisable();

            _inventory.InventoryChangedEvent -= UpdateTool;
        }

        private void UpdateTool() {
            var selectedItem = _inventory.GetSelectedHotbarItem();
            var toolPrefab = selectedItem == null ? null : _itemsInfo.GetInfo(selectedItem.itemId).ToolPrefab;
            if (_currentToolPrefab != toolPrefab) {
                _currentToolPrefab = toolPrefab;
                if (_currentTool != null) {
                    Destroy(_currentTool);
                    IsUsing = false;
                }

                if (toolPrefab != null) {
                    _currentTool = _instantiator.InstantiatePrefab(toolPrefab);
                    _currentTool.transform.SetParent(transform, false);
                }
            }
        }
    }
}