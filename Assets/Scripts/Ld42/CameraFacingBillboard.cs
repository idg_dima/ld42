﻿using UnityEngine;
using Zenject;

namespace Ld42 {
    public class CameraFacingBillboard : MonoBehaviour {

        [Inject] private Camera _camera;

        private void Update() {
            transform.LookAt(transform.position + _camera.transform.rotation * Vector3.forward, _camera.transform.rotation * Vector3.up);
        }
    }
}