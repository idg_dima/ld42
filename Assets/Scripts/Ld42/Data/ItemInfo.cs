﻿using System;
using UnityEngine;

namespace Ld42.Data {
    [Serializable]
    public class ItemInfo {
        public ItemIds Id;
        public ToolTypes ToolType;
        public float ToolEfficiency;
        public string Name;
        public string Description;
        public Sprite Sprite;
        public GameObject ToolPrefab;
        public GameObject PlacedPrefab;
        public GameObject ConveyorPrefab;
    }
}