﻿using System;

namespace Ld42.Data {
    [Serializable]
    public class CraftingIngredient {
        public ItemIds itemId;
        public int itemCount;
    }
}