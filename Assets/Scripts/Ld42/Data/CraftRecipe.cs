﻿using System;

namespace Ld42.Data {
    [Serializable]
    public class CraftRecipe {
        public ItemIds ResultItemId;
        public int ResultItemCount;
        public CraftingIngredient[] Ingredients;
    }
}