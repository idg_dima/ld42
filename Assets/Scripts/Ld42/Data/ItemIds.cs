﻿namespace Ld42.Data {
    public enum ItemIds {
        Wood = 1,
        Stone = 2,
        StoneAxe = 3,
        StonePickaxe = 4,
        IronAxe = 5,
        IronPickaxe = 6,
        Coal = 7,
        IronOre = 8,
        IronIngot = 9,
        Furnace = 10,
        Conveyor = 11,
        Crate = 12,
        AutoMiner = 13,
        LaunchPad = 14,
    }
}