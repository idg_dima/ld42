﻿using System.Collections.Generic;
using UnityEngine;

namespace Ld42.Data {

    [CreateAssetMenu(fileName = "ItemsInfo", menuName = "Data/ItemsInfo")]
    public class ItemsInfo : ScriptableObject {
        private Dictionary<ItemIds, ItemInfo> _infoById;

        public ItemInfo[] Items;

        private void OnEnable() {
            _infoById = new Dictionary<ItemIds, ItemInfo>();
            foreach (var item in Items) {
                _infoById[item.Id] = item;
            }
        }

        public ItemInfo GetInfo(ItemIds itemId) {
            return _infoById[itemId];
        }
    }
}