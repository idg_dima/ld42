﻿using UnityEngine;

namespace Ld42.Data {

    [CreateAssetMenu(fileName = "CraftsInfo", menuName = "Data/CraftsInfo")]
    public class CraftsInfo : ScriptableObject {
        public CraftRecipe[] CraftingRecipes;
    }
}