﻿using System;
using System.Linq;
using Ld42.Data;

namespace Ld42 {
    public class Inventory {
        public const int HotbarSize = 5;
        public const int SlotsNum = 15;

        public event Action InventoryChangedEvent = () => {};
        public event Action InventoryFullEvent = () => {};

        public InventorySlot DraggingSlot { get; private set; }
        public int SelectedHotbarSlotIndex { get; private set; }

        private InventorySlot[] _slots;

        public Inventory() {
            _slots = new InventorySlot[SlotsNum];


            //todo: remove
//            AddItem(ItemIds.Wood, 100);
//            AddItem(ItemIds.Stone, 100);
//            AddItem(ItemIds.StoneAxe, 100);
//            AddItem(ItemIds.StonePickaxe, 100);
//            AddItem(ItemIds.IronAxe, 100);
//            AddItem(ItemIds.IronPickaxe, 100);
//            AddItem(ItemIds.Coal, 100);
//            AddItem(ItemIds.IronOre, 100);
//            AddItem(ItemIds.IronIngot, 100);
//            AddItem(ItemIds.Furnace, 100);
//            AddItem(ItemIds.Conveyor, 100);
//            AddItem(ItemIds.Crate, 100);
//            AddItem(ItemIds.AutoMiner, 100);
//            AddItem(ItemIds.LaunchPad, 100);

        }

        public int AddItem(ItemIds itemId, int count) {
            var itemsToAdd = AddItemsToExistingSlots(itemId, count);
            var extraItems = AddItemsToNewSlots(itemId, itemsToAdd);

            InventoryChangedEvent();
            return extraItems;
        }

        private int AddItemsToExistingSlots(ItemIds itemId, int count) {
            var itemsToAdd = count;
            var slotsWithItem = _slots.Where(s => s != null && s.itemId == itemId);
            foreach (var slot in slotsWithItem) {
                itemsToAdd = slot.AddItems(itemsToAdd);
                if (itemsToAdd == 0) {
                    return 0;
                }
            }

            return itemsToAdd;
        }

        private int AddItemsToNewSlots(ItemIds itemId, int count) {
            var itemsToAdd = count;

            try {
                while (itemsToAdd != 0) {
                    var slot = CreateNewSlot(itemId);
                    itemsToAdd = slot.AddItems(count);
                }
            } catch (InventoryFullException) {
                InventoryFullEvent();
            }

            return itemsToAdd;
        }

        private InventorySlot CreateNewSlot(ItemIds itemId) {
            for (var i = 0; i < SlotsNum; i++) {
                if (_slots[i] == null) {
                    _slots[i] = new InventorySlot(itemId);
                    return _slots[i];
                }
            }
            throw new InventoryFullException();
        }

        public bool HasItem(ItemIds itemId, int count) {
            return CountItem(itemId) >= count;
        }

        private int CountItem(ItemIds itemId) {
            return _slots
                .Where(s => s != null)
                .Where(s => s.itemId == itemId)
                .Sum(s => s.count);
        }

        public void RemoveItem(ItemIds itemId, int count) {
            var itemsToRemove = count;
            for (var i = 0; i < SlotsNum; i++) {
                if (_slots[i] != null && _slots[i].itemId == itemId) {
                    var newCount = _slots[i].count - itemsToRemove;
                    if (newCount > 0) {
                        _slots[i].count = newCount;
                        InventoryChangedEvent();
                        return;
                    }

                    itemsToRemove -= _slots[i].count;
                    _slots[i] = null;
                }
            }
            InventoryChangedEvent();
        }

        public void RemoveFromSelectedHotbar(int count) {
            var slot = _slots[SelectedHotbarSlotIndex];
            if (slot == null) {
                return;
            }

            slot.count -= count;
            if (slot.count <= 0) {
                _slots[SelectedHotbarSlotIndex] = null;
            }
            InventoryChangedEvent();
        }

        public InventorySlot GetSlot(int index) {
            return _slots[index];
        }

        public void StartDraggingItem(int index, bool moveAll) {
            if (_slots[index] == null || DraggingSlot != null) {
                return;
            }

            if (moveAll || _slots[index].count == 1) {
                DraggingSlot = _slots[index];
                _slots[index] = null;
                InventoryChangedEvent();
            } else {
                var itemsToMove = TakeHalf(_slots[index]);
                DraggingSlot = new InventorySlot(_slots[index].itemId, itemsToMove);
                InventoryChangedEvent();
            }
        }

        public void PlaceDraggingItemTo(int index, bool placeAll) {
            if (DraggingSlot == null) {
                return;
            }

            if (_slots[index] == null) {
                if (placeAll || DraggingSlot.count == 1) {
                    _slots[index] = DraggingSlot;
                    DraggingSlot = null;
                } else {
                    DraggingSlot.count--;
                    _slots[index] = new InventorySlot(DraggingSlot.itemId, 1);
                }
            } else if (_slots[index].itemId == DraggingSlot.itemId) {
                if (placeAll || DraggingSlot.count == 1) {
                    var extraItems = _slots[index].AddItems(DraggingSlot.count);
                    if (extraItems == 0) {
                        DraggingSlot = null;
                    } else {
                        DraggingSlot.count = extraItems;
                    }
                } else {
                    DraggingSlot.count--;
                    var extraItems = _slots[index].AddItems(1);
                    if (extraItems != 0) {
                        DraggingSlot.AddItems(extraItems);
                    }
                }
            } else {
                var tmp = _slots[index];
                _slots[index] = DraggingSlot;
                DraggingSlot = tmp;
            }

            InventoryChangedEvent();
        }

        public void CancelDraggingItem() {
            if (DraggingSlot != null) {
                AddItem(DraggingSlot.itemId, DraggingSlot.count);
                DraggingSlot = null;
                InventoryChangedEvent();
            }
        }

        private int TakeHalf(InventorySlot slot) {
            var itemsToLeave = slot.count / 2;
            var itemsToTake = slot.count - itemsToLeave;
            slot.count = itemsToLeave;
            return itemsToTake;
        }

        public void SelectHotbarSlot(int index) {
            SelectedHotbarSlotIndex = index;
            InventoryChangedEvent();
        }

        public InventorySlot GetSelectedHotbarItem() {
            return _slots[SelectedHotbarSlotIndex];
        }

        public bool CheckFreeSlotsAndTriggerEvent(ItemIds itemIds, int count) {
            var freeSpace = 0;
            foreach (var slot in _slots) {
                if (slot == null) {
                    freeSpace += InventorySlot.MaxStackSize;
                } else if (slot.itemId == itemIds) {
                    freeSpace += InventorySlot.MaxStackSize - slot.count;
                }
            }

            if (freeSpace < count) {
                InventoryFullEvent();
            }
            return freeSpace >= count;
        }

        public void DestroyDraggingItem(bool destroyAll) {
            if (DraggingSlot == null) {
                return;
            }

            if (destroyAll || DraggingSlot.count == 1) {
                DraggingSlot = null;
            } else {
                DraggingSlot.count--;
            }

            InventoryChangedEvent();
        }
    }
}