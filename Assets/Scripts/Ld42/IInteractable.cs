﻿namespace Ld42 {
    public interface IInteractable {
        InteractionType GetInteractionType(InventorySlot selectedItem);

        string GetActionName(InventorySlot selectedItem);

        float GetInteractionTime(InventorySlot selectedItem);

        void Interact(InventorySlot selectedItem);
    }
}