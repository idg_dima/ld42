﻿using Ld42.Data;

namespace Ld42 {
    public class InventorySlot {
        public const int MaxStackSize = 100;

        public ItemIds itemId;
        public int count;

        public InventorySlot(ItemIds itemId, int count = 0) {
            this.itemId = itemId;
            this.count = count;
        }

        /// <summary>
        /// Returns extra items that exceed max stack size
        /// </summary>
        public int AddItems(int newItems) {
            var newCount = count + newItems;
            if (newCount > MaxStackSize) {
                var extraItems = newCount - MaxStackSize;
                count = MaxStackSize;
                return extraItems;
            }

            count = newCount;
            return 0;
        }
    }
}