﻿using System.Collections;
using Ld42.Data;
using UnityEngine;
using Zenject;

namespace Ld42 {
    public class Furnace : AbstractDestroyableMachine, IMachineWithInventory {
        private const float TimeToProcess = 5;
        private const float LightMinIntensity = 2.8f;
        private const float LightMaxIntensity = 3.2f;

        [Inject] private Inventory _inventory;

        [SerializeField] private Light _light;

        private bool _hasCoal;
        private bool _hasOre;
        private bool _hasIngot;
        private bool _isProcessing;

        public override InteractionType GetInteractionType(InventorySlot selectedItem) {
            if (_hasIngot) {
                return InteractionType.Take;
            }

            if (selectedItem == null) {
                return InteractionType.None;
            }

            switch (selectedItem.itemId) {
                case ItemIds.Coal:
                    return _hasCoal ? InteractionType.None : InteractionType.Use;
                case ItemIds.IronOre:
                    return _hasOre ? InteractionType.None : InteractionType.Use;
                default:
                    return base.GetInteractionType(selectedItem);
            }
        }

        public override string GetActionName(InventorySlot selectedItem) {
            if (_hasIngot) {
                return "Take ingot";
            }

            if (selectedItem == null) {
                return "";
            }

            switch (selectedItem.itemId) {
                case ItemIds.Coal:
                case ItemIds.IronOre:
                    return "Place";
                default:
                    return base.GetActionName(selectedItem);
            }
        }

        public override float GetInteractionTime(InventorySlot selectedItem) {
            if (_hasIngot) {
                return 0;
            }

            if (selectedItem == null) {
                return 0;
            }

            switch (selectedItem.itemId) {
                case ItemIds.Coal:
                case ItemIds.IronOre:
                    return 0;
                default:
                    return base.GetInteractionTime(selectedItem);
            }
        }

        private void TryProcess() {
            if (_isProcessing || !_hasCoal || !_hasOre || _hasIngot) {
                return;
            }

            StartCoroutine(ProcessCoroutine());
        }

        private IEnumerator ProcessCoroutine() {
            _isProcessing = true;
            _hasCoal = false;
            _hasOre = false;
            _light.gameObject.SetActive(true);

            var timeLeft = TimeToProcess;
            yield return null;

            while (timeLeft > 0) {
                timeLeft -= Time.deltaTime;
                _light.intensity = Random.Range(LightMinIntensity, LightMaxIntensity);
                yield return null;
            }

            _hasIngot = true;
            _isProcessing = false;
            _light.gameObject.SetActive(false);
        }

        public override void Interact(InventorySlot selectedItem) {
            if (_hasIngot) {
                if (_inventory.CheckFreeSlotsAndTriggerEvent(ItemIds.IronIngot, 1)) {
                    _hasIngot = false;
                    _inventory.AddItem(ItemIds.IronIngot, 1);
                    TryProcess();
                }
                return;
            }

            if (selectedItem == null) {
                return;
            }

            switch (selectedItem.itemId) {
                case ItemIds.Coal:
                    if (!_hasCoal) {
                        _inventory.RemoveFromSelectedHotbar(1);
                        _hasCoal = true;
                        TryProcess();
                    }
                    return;
                case ItemIds.IronOre:
                    if (!_hasOre) {
                        _inventory.RemoveFromSelectedHotbar(1);
                        _hasOre = true;
                        TryProcess();
                    }
                    return;
                default:
                    base.Interact(selectedItem);
                    return;
            }
        }

        protected override bool GiveDestroyResources() {
            if (_inventory.CheckFreeSlotsAndTriggerEvent(ItemIds.Furnace, 1)) {
                _inventory.AddItem(ItemIds.Furnace, 1);
                return true;
            }

            return false;
        }

        public bool CanTakeOneItem() {
            return _hasIngot;
        }

        public ItemIds TakeOneItem() {
            _hasIngot = false;
            TryProcess();
            return ItemIds.IronIngot;
        }

        public bool CanPlaceOneItem(ItemIds itemId) {
            return itemId == ItemIds.Coal && !_hasCoal || itemId == ItemIds.IronOre && !_hasOre;
        }

        public void PlaceOneItem(ItemIds itemId) {
            if (itemId == ItemIds.Coal) {
                _hasCoal = true;
            } else if (itemId == ItemIds.IronOre) {
                _hasOre = true;
            }
            TryProcess();
        }
    }
}