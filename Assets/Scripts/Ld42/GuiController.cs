﻿using Ld42.Gui.InventoryGui;
using UnityEngine;
using Zenject;

namespace Ld42 {
    public class GuiController : MonoBehaviour {
        [Inject] private Player _player;
        [Inject] private InventoryGui _inventoryGui;

        public bool IsInventoryOpenned { get; private set; }

        private void Start() {
            InventoryVisible = false;
        }

        private void Update() {

            if (Input.GetButtonDown("Inventory")) {
                InventoryVisible = !IsInventoryOpenned;
            }

        }

        private bool InventoryVisible {
            set {
                IsInventoryOpenned = value;
                _inventoryGui.IsVisible = value;
                _player.Paused = value;
            }
        }
    }
}