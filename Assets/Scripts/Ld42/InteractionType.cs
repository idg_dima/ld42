﻿namespace Ld42 {
    public enum InteractionType {
        None,
        Extract,
        Use,
        Take,
        Place,
    }
}