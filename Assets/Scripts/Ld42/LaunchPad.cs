﻿using System.Collections;
using Ld42.Data;
using UnityEngine;
using Zenject;

namespace Ld42 {
    public class LaunchPad : AbstractDestroyableMachine, IMachineWithInventory {
        private const float RotationTime = 0.15f;
        private const float MoveTime = 1f;

        [Inject] private ItemsInfo _itemsInfo;
        [Inject] private Inventory _inventory;
        [Inject] private IInstantiator _instantiator;

        [SerializeField] private Transform _backRaycastPoint;
        [SerializeField] private Transform _frontRaycastPoint;
        [SerializeField] private Transform _movingObjectContainer;
        [SerializeField] private AnimationCurve _yAnimationCurve;
        [SerializeField] private float _amplitude;

        private int _raycastMask;
        private bool _isRotating;
        private int _rotation;
        private GameObject _contentsGameObject;
        private bool _isMoving;
        private ItemIds? _contents;

        private void Awake() {
            _raycastMask = LayerMask.GetMask("Interactive");
            StartCoroutine(UpdateCoroutine());
        }

        protected override bool GiveDestroyResources() {
            if (_inventory.CheckFreeSlotsAndTriggerEvent(ItemIds.Conveyor, 1)) {
                _inventory.AddItem(ItemIds.Conveyor, 1);
                return true;
            }

            return false;
        }

        public override InteractionType GetInteractionType(InventorySlot selectedItem) {
            if (CanDestroyWith(selectedItem)) {
                return base.GetInteractionType(selectedItem);
            }

            return InteractionType.Use;
        }

        public override string GetActionName(InventorySlot selectedItem) {
            if (CanDestroyWith(selectedItem)) {
                return base.GetActionName(selectedItem);
            }
            return "Rotate";
        }

        public override void Interact(InventorySlot selectedItem) {
            if (CanDestroyWith(selectedItem)) {
                base.Interact(selectedItem);
                return;
            }

            if (!_isRotating) {
                StartCoroutine(RotateCoroutine());
            }
        }

        private IEnumerator RotateCoroutine() {
            _isRotating = true;

            var timer = 0f;
            while (timer < RotationTime) {
                timer += Time.deltaTime;
                var angle = Mathf.Lerp(_rotation, _rotation + 1, timer / RotationTime) * 90;
                transform.localRotation = Quaternion.Euler(0, angle, 0);
                yield return null;
            }

            _rotation++;
            _isRotating = false;
        }

        private IEnumerator UpdateCoroutine() {
            while (true) {
                if (!_isRotating && !_isMoving) {
                    if (_contents == null) {
                        var backMachine = GetBackMachine();
                        if (backMachine != null && backMachine.CanTakeOneItem()) {
                            _contents = backMachine.TakeOneItem();
                            StartCoroutine(MoveCoroutine());
                        }
                    }
                }

                yield return new WaitForSeconds(1);
            }
        }

        private void TryPlaceContents() {
            if (_contents == null) {
                return;
            }

            var frontMachine = GetFrontMachine();
            if (frontMachine != null && frontMachine.CanPlaceOneItem(_contents.Value)) {
                frontMachine.PlaceOneItem(_contents.Value);
            }

            _contents = null;
            Destroy(_contentsGameObject);
            _contentsGameObject = null;
        }

        private IEnumerator MoveCoroutine() {
            if (_contents != null) {
                var itemInfo = _itemsInfo.GetInfo(_contents.Value);
                _contentsGameObject = _instantiator.InstantiatePrefab(itemInfo.ConveyorPrefab);
                _contentsGameObject.transform.SetParent(_movingObjectContainer, false);
                _contentsGameObject.transform.localPosition = Vector3.zero;
                _contentsGameObject.transform.localRotation = Quaternion.identity;

                _isMoving = true;

                var timer = 0f;
                while (timer < MoveTime) {
                    timer += Time.deltaTime;
                    var progress = timer / MoveTime;
                    var positionZ = Mathf.Lerp(-0.5f, 7.5f, progress);
                    var positionY = Mathf.Lerp(0, _amplitude, _yAnimationCurve.Evaluate(progress));
                    _movingObjectContainer.localPosition = new Vector3(_movingObjectContainer.localPosition.x, positionY, positionZ);
                    yield return null;
                }

                _isMoving = false;
                TryPlaceContents();
            }
        }

        private IMachineWithInventory GetBackMachine() {
            RaycastHit hitInfo;
            if (Physics.Raycast(_backRaycastPoint.position, transform.forward * -1, out hitInfo, 0.1f, _raycastMask)) {
                return hitInfo.collider.gameObject.GetComponent<IMachineWithInventory>();
            }

            return null;
        }

        private IMachineWithInventory GetFrontMachine() {
            RaycastHit hitInfo;
            if (Physics.Raycast(_frontRaycastPoint.position, transform.forward, out hitInfo, 0.1f, _raycastMask)) {
                return hitInfo.collider.gameObject.GetComponent<IMachineWithInventory>();
            }

            return null;
        }


        public bool CanTakeOneItem() {
            return false;
        }

        public ItemIds TakeOneItem() {
            return ItemIds.Stone;
        }

        public bool CanPlaceOneItem(ItemIds itemId) {
            return _contents == null;
        }

        public void PlaceOneItem(ItemIds itemId) {
            _contents = itemId;
            StartCoroutine(MoveCoroutine());
        }
    }
}