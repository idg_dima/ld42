﻿using Ld42.Data;
using UnityEngine;
using Zenject;

namespace Ld42 {
    public class Crate : AbstractDestroyableMachine, IMachineWithInventory {
        [Inject] private Inventory _inventory;

        [SerializeField] private CrateItemsInfo _crateItemsInfo;

        private InventorySlot _contents;

        private void Awake() {
            _crateItemsInfo.IsVisible = true;
        }

        protected override bool GiveDestroyResources() {
            if (_inventory.CheckFreeSlotsAndTriggerEvent(ItemIds.Crate, 1)) {
                _inventory.AddItem(ItemIds.Crate, 1);
                return true;
            }

            return false;
        }

        public override InteractionType GetInteractionType(InventorySlot selectedItem) {
            if (CanDestroyWith(selectedItem)) {
                return base.GetInteractionType(selectedItem);
            }

            if (_contents == null) {
                return selectedItem == null ? InteractionType.None : InteractionType.Use;
            }

            if (selectedItem == null) {
                return InteractionType.Take;
            }
            return _contents.itemId == selectedItem.itemId ? InteractionType.Use : InteractionType.Take;
        }

        public override string GetActionName(InventorySlot selectedItem) {
            if (CanDestroyWith(selectedItem)) {
                return base.GetActionName(selectedItem);
            }

            if (_contents == null) {
                return selectedItem == null ? "" : "Place items";
            }

            if (selectedItem == null) {
                return "Take items";
            }
            return _contents.itemId == selectedItem.itemId ? "Place items" : "Take items";
        }

        public override void Interact(InventorySlot selectedItem) {
            if (CanDestroyWith(selectedItem)) {
                base.Interact(selectedItem);
                return;
            }

            if (_contents == null) {
                if (selectedItem == null) {
                    return;
                }
                _contents = new InventorySlot(selectedItem.itemId, selectedItem.count);
                _inventory.RemoveFromSelectedHotbar(selectedItem.count);
                _crateItemsInfo.UpdateItem(_contents);
                return;
            }

            if (selectedItem == null) {
                MoveContentsToInventory();
                return;
            }

            if (_contents.itemId == selectedItem.itemId) {
                var extraItems = _contents.AddItems(selectedItem.count);
                _inventory.RemoveFromSelectedHotbar(selectedItem.count - extraItems);
                _crateItemsInfo.UpdateItem(_contents);
            } else {
                MoveContentsToInventory();
            }
        }

        private void MoveContentsToInventory() {
            var extraItems = _inventory.AddItem(_contents.itemId, _contents.count);
            if (extraItems == 0) {
                _contents = null;
            } else {
                _contents.count = extraItems;
            }
            _crateItemsInfo.UpdateItem(_contents);
        }

        public bool CanTakeOneItem() {
            return _contents != null;
        }

        public ItemIds TakeOneItem() {
            var itemId = _contents.itemId;
            _contents.count--;
            if (_contents.count <= 0) {
                _contents = null;
            }
            _crateItemsInfo.UpdateItem(_contents);
            return itemId;
        }

        public bool CanPlaceOneItem(ItemIds itemId) {
            if (_contents == null) {
                return true;
            }

            return _contents.itemId == itemId && _contents.count < InventorySlot.MaxStackSize;
        }

        public void PlaceOneItem(ItemIds itemId) {
            if (_contents == null) {
                _contents = new InventorySlot(itemId, 1);
            } else if (_contents.itemId == itemId) {
                _contents.AddItems(1);
            }
            _crateItemsInfo.UpdateItem(_contents);
        }
    }
}