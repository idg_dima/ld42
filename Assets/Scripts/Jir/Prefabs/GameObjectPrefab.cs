﻿using UnityEngine;
using Zenject;

namespace Jir.Prefabs {
    public class GameObjectPrefab : Prefab<GameObject> {

        public GameObjectPrefab(IInstantiator instantiator, string resourcePath)
            : base(instantiator, resourcePath) {
        }

        public override GameObject Instantiate() {
            return Instantiator.InstantiatePrefab(PrefabObject);
        }
    }
}