﻿using UnityEngine;
using Zenject;

namespace Jir.Prefabs {
    public class Prefab<T> : IPrefab {
        protected readonly IInstantiator Instantiator;
        protected readonly GameObject PrefabObject;

        public Prefab(IInstantiator instantiator, string resourcePath) {
            Instantiator = instantiator;

            PrefabObject = Resources.Load<GameObject>(resourcePath);
        }

        public virtual T Instantiate() {
            return Instantiator.InstantiatePrefabForComponent<T>(PrefabObject);
        }

        public object InstantiateObject() {
            return Instantiate();
        }
    }
}